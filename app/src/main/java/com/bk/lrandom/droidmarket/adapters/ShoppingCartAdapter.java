package com.bk.lrandom.droidmarket.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bk.lrandom.droidmarket.R;
import com.bk.lrandom.droidmarket.models.Cart;
import com.gc.materialdesign.views.Button;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

/**
 * Created by Andrea's on 6/25/2016.
 */
public class ShoppingCartAdapter extends RecyclerView.Adapter<ShoppingCartAdapter.CartsViewHolder> {

    private Context context;
    private ArrayList<Cart> carts;
    private static final int FOOTER_VIEW = 1;

    public ShoppingCartAdapter(Context context, ArrayList<Cart> carts) {
        this.context = context;
        this.carts = carts;
    }

    public void swapData(ArrayList<Cart> data){
        carts.clear();
        carts.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public CartsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == FOOTER_VIEW){
            view = LayoutInflater.from(context).inflate(R.layout.footer_shopping_cart_rv, parent, false);
            FooterViewHolder holder = new FooterViewHolder(view);
            return holder;
        }
        view = LayoutInflater.from(context).inflate(R.layout.card_item_list_cart, parent, false);
        NormalViewHolder holder = new NormalViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CartsViewHolder holder, int position) {
        try {
            if (holder instanceof  NormalViewHolder){
                NormalViewHolder vh = (NormalViewHolder) holder;
                vh.bindNormalView(position);
            } else if (holder instanceof FooterViewHolder){
                FooterViewHolder vh = (FooterViewHolder) holder;
                vh.bindFooterView(position);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == carts.size()){
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (carts == null){
            return 0;
        }

        if (carts.size() == 0){
            return 1;
        }

        return carts.size() + 1;
    }

    public class CartsViewHolder extends RecyclerView.ViewHolder{
        private TextView tvProductName, tvStoreName, tvPrice;
        private EditText qtyEdt;
        private ImageView productImage;
        private Button submitButton;

        public CartsViewHolder(View itemView) {
            super(itemView);
            tvProductName = (TextView) itemView.findViewById(R.id.product_name_tv);
            tvStoreName = (TextView) itemView.findViewById(R.id.store_name_tv);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            qtyEdt = (EditText) itemView.findViewById(R.id.amount_edt);
            productImage = (ImageView) itemView.findViewById(R.id.thumb);
            submitButton = (Button) itemView.findViewById(R.id.submit_btn);
        }

        public void bindNormalView(int position){
            tvProductName.setText(carts.get(position).getProductName());
            tvStoreName.setText(carts.get(position).getStoreName());
            qtyEdt.setText(String.valueOf(carts.get(position).getProductAmount()));
            tvPrice.setText(String.valueOf(carts.get(position).getProductPrice()));
            Ion.with(
                    context,
                    context.getResources().getString(R.string.domain_url)).withBitmap()
                    .resize(200, 200).centerCrop().error(R.drawable.no_photo)
                    .placeholder(R.drawable.no_photo).intoImageView(productImage);
        }

        public void bindFooterView(int position){
            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Hello", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public class FooterViewHolder extends CartsViewHolder{
        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class NormalViewHolder extends CartsViewHolder{
        public NormalViewHolder(View itemView) {
            super(itemView);
        }
    }


}
