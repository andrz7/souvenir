package com.bk.lrandom.droidmarket.models;

/**
 * Created by Andrea's on 6/26/2016.
 */
public class Cart {
    private String productName,  storeName;
    private long productPrice;
    private int productAmount;

    public Cart(String productName, String storeName, long productPrice) {
        this.productName = productName;
        this.storeName = storeName;
        this.productPrice = productPrice;
        this.productAmount = 1;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public long getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(long productPrice) {
        this.productPrice = productPrice;
    }

    public int getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(int productAmount) {
        this.productAmount = productAmount;
    }
}
