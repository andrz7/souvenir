package com.bk.lrandom.droidmarket;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.bk.lrandom.droidmarket.business.Utils;
import com.bk.lrandom.droidmarket.business.UserSessionManager;
import com.bk.lrandom.droidmarket.models.User;
import com.gc.materialdesign.views.ButtonRectangle;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends ActionBarParentActivity implements View.OnClickListener {
	private final String TAG = LoginActivity.class.getSimpleName();

	private TextView email, pwd, createAccount;
	private ButtonRectangle btnLogin;
	private ProgressDialog dialogPrg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
		initView();

		dialogPrg = new ProgressDialog(this);
		dialogPrg.setCanceledOnTouchOutside(false);

		btnLogin.setOnClickListener(this);
		createAccount.setOnClickListener(this);
	}

	private void initView(){
		createAccount = (TextView) findViewById(R.id.create_account_tv);
		btnLogin = (ButtonRectangle) findViewById(R.id.btn_login);
		email = (TextView) findViewById(R.id.email);
		pwd = (TextView) findViewById(R.id.pwd);
	}

	public void showDialogFailedLogin() {
		Utils.logout(LoginActivity.this);
        showDialog(getResources().getString(R.string.login_failed));
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.btn_login:{
				if (!Utils.isConnectingToInternet(LoginActivity.this)) {
					Toast.makeText(LoginActivity.this,
							getResources().getString(R.string.open_network),
							Toast.LENGTH_LONG).show();
				} else {
					dialogPrg.setMessage(getResources().getString(
							R.string.loging));
					dialogPrg.show();
				}
			}
				break;
			case R.id.create_account_tv:
				startActivity(new Intent(this, CreateAccountActivity.class));
				finish();
				break;
		}
	}
}
