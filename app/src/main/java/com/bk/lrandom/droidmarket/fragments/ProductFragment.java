package com.bk.lrandom.droidmarket.fragments;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import com.bk.lrandom.droidmarket.DetailActivity;
import com.bk.lrandom.droidmarket.R;
import com.bk.lrandom.droidmarket.adapters.ProductsAdapter;
import com.bk.lrandom.droidmarket.confs.constants;
import com.bk.lrandom.droidmarket.models.DummyData;
import com.bk.lrandom.droidmarket.models.Products;

public class ProductFragment extends Fragment implements OnClickListener{
    private static final String TAG = ProductFragment.class.getSimpleName();
    ArrayList<Products> products_list;
    ProductsAdapter adapter;
    LinearLayout loadMorePrg;
    boolean loadingMore = true;
    SwipeRefreshLayout swipeRefreshLayout;
    Button btnFood, btnClothes, btnSouvenir;
    RecyclerView productRecycler;
    int user_id = 0, user_post = 0, pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
    }

    public static final ProductFragment newInstance() {
        // TODO Auto-generated constructor stub
        ProductFragment fragment = new ProductFragment();
        return fragment;
    }

    @SuppressWarnings("deprecation")
    private void setButtonFocus(Button btn, int drawable) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            btn.setBackgroundDrawable(getActivity().getResources().getDrawable(
                    drawable));
        } else {
            btn.setBackground(getActivity().getResources()
                    .getDrawable(drawable));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;
        view = inflater.inflate(R.layout.listview_endless_container_layout, null);
        productRecycler = (RecyclerView) view.findViewById(R.id.rv);
        loadMorePrg = (LinearLayout) view.findViewById(R.id.prgLoadMore);
        btnFood = (Button) view.findViewById(R.id.btnAll);
        btnClothes = (Button) view.findViewById(R.id.btnSell);
        btnSouvenir = (Button) view.findViewById(R.id.btnBuy);
        setButtonFocus(btnFood, R.drawable.tab_categories_pressed);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.activity_main_swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (products_list != null && products_list.size() != 0) {

                }
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final LinearLayoutManager llm = new LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false
        );
        productRecycler.setLayoutManager(llm);
        products_list = new DummyData("food").getData();
        adapter = new ProductsAdapter(getActivity(), products_list);
        adapter.SetOnItemClickListener(new ProductsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (user_id == 0) {
                    Intent intent = new Intent(getActivity(), DetailActivity.class);
                    intent.putExtra(constants.COMMON_KEY, products_list.get(position)
                            .getId());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getActivity(), DetailActivity.class);
                    intent.putExtra(constants.COMMON_KEY, products_list.get(position)
                            .getId());
                    intent.putExtra(constants.USER_ID_KEY, user_id);
                    startActivity(intent);
                }
            }
        });
        productRecycler.setAdapter(adapter);
        productRecycler.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = llm.getChildCount();
                totalItemCount = llm.getItemCount();
                pastVisiblesItems = llm.findFirstVisibleItemPosition();
                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    loadingMore = false;
                    Log.v("...", "Last Item Wow !");
                }
            }
        } );
        btnFood.setOnClickListener(this);
        btnSouvenir.setOnClickListener(this);
        btnClothes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAll:
                foodEventClicked();
                break;
            case R.id.btnBuy:
                souvenirEventClicked();
                break;
            case R.id.btnSell:
                clothesEventClicked();
                break;
        }
    }

    private void souvenirEventClicked() {
        setButtonFocus(btnFood, R.drawable.tab_categories_normal);
        setButtonFocus(btnClothes, R.drawable.tab_categories_normal);
        setButtonFocus(btnSouvenir, R.drawable.tab_categories_pressed);
        adapter.swapData(new DummyData("souvenir").getData());
    }

    private void clothesEventClicked() {
        setButtonFocus(btnFood, R.drawable.tab_categories_normal);
        setButtonFocus(btnClothes, R.drawable.tab_categories_pressed);
        setButtonFocus(btnSouvenir, R.drawable.tab_categories_normal);
        adapter.swapData(new DummyData("clothes").getData());
    }

    private void foodEventClicked() {
        setButtonFocus(btnFood, R.drawable.tab_categories_pressed);
        setButtonFocus(btnClothes, R.drawable.tab_categories_normal);
        setButtonFocus(btnSouvenir, R.drawable.tab_categories_normal);
        adapter.swapData(new DummyData("food").getData());
    }

}
