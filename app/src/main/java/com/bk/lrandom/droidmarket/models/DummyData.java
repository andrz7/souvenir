package com.bk.lrandom.droidmarket.models;

import java.util.ArrayList;

/**
 * Created by Andrea's on 6/14/2016.
 */
public class DummyData {
    private ArrayList<Products> productsList;

    public DummyData(String type) {
        productsList = new ArrayList<>();
        if (type.equalsIgnoreCase("food")){
            for (int i = 1 ; i <= 3 ; i++)
                productsList.add(new Products(i, "Food" + i, "20000", "pademangan", "pademangan", 1, "food", 1, 1
                        , "zero", "date", "content", "currency", "jakarta"));
        }else if (type.equalsIgnoreCase("clothes")){
            for (int i = 0 ; i <= 3 ; i++)
                productsList.add(new Products(i, "Clothes" + i, null, "pademangan", "pademangan", 1, "food", 1, 1
                        , "zero", "date", "content", "currency", "jakarta"));
        }else{
            for (int i = 0 ; i <= 3 ; i++)
                productsList.add(new Products(i, "Souvenir" + i, null, "pademangan", "pademangan", 1, "food", 1, 1
                        , "zero", "date", "content", "currency", "jakarta"));
        }

    }

    public ArrayList<Cart> dummyCart(){
        ArrayList<Cart> carts = new ArrayList<>();
        for (int i = 1 ; i <= 10 ; i++){
            carts.add(new Cart("Product " + i, "Store " + i, 10000));
        }
        return carts;
    }

    public ArrayList<Products> getData(){
        return productsList;
    }

}
