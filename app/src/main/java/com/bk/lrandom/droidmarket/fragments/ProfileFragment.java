package com.bk.lrandom.droidmarket.fragments;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.widgets.Dialog;

import com.bk.lrandom.droidmarket.ChangePassActivity;
import com.bk.lrandom.droidmarket.UpdateProfileActivity;
import com.bk.lrandom.droidmarket.ProductsActivity;
import com.bk.lrandom.droidmarket.R;
import com.bk.lrandom.droidmarket.business.RoundedAvatarDrawable;
import com.bk.lrandom.droidmarket.business.Utils;
import com.bk.lrandom.droidmarket.business.UserSessionManager;
import com.bk.lrandom.droidmarket.confs.constants;
import com.bk.lrandom.droidmarket.interfaces.ProfileComunicator;
import com.bk.lrandom.droidmarket.models.User;
import com.gc.materialdesign.views.ButtonRectangle;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class ProfileFragment extends Fragment {
	private final String TAG = ProfileFragment.class.getSimpleName();
    ButtonRectangle btnEdit,btnLogout, btnShowMyProducts,
            btnChangePass, btnUpdateProduct;
	EditText displayName, email, website, address, phone, userName;
	ImageView avt;
	ProfileComunicator listener;
	ProgressDialog loadingDialog;
	User userProfile;
	ProgressDialog prgDialog;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		listener = (ProfileComunicator) getActivity();
	}

	public static final ProfileFragment newInstance() {
		// TODO Auto-generated constructor stub
		ProfileFragment fragment = new ProfileFragment();
		return fragment;
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		UserSessionManager sessionManager = new UserSessionManager(
				getActivity());
		// userProfile = new User();
		userProfile = sessionManager.getUserSession();
		if (userProfile != null) {
			displayName.setText(userProfile.getFullName());
			website.setText(userProfile.getWebsite());
			phone.setText(userProfile.getPhone());
			address.setText(userProfile.getAddress());
			email.setText(userProfile.getEmail());
			userName.setText(userProfile.getUserName());
			if (userProfile.getAvt() != null
					&& !userProfile.getAvt().equalsIgnoreCase("")) {
				Log.i(TAG, "Khac null");
				String avtString = "";
				if (Utils.checkFacebookAvt(userProfile.getAvt())) {
					avtString = userProfile.getAvt();
				} else {
					avtString = getResources().getString(R.string.domain_url)
							+ userProfile.getAvt();
				}
				Log.i(TAG, avtString);
				Ion.with(getActivity(), avtString).withBitmap()
						.resize(200, 200).centerCrop()
						.placeholder(R.drawable.ic_avatar)
						.error(R.drawable.ic_avatar).asBitmap()
						.setCallback(new FutureCallback<Bitmap>() {

							@Override
							public void onCompleted(Exception arg0,
									Bitmap bitmap) {
								// TODO Auto-generated method stub
								if (bitmap != null) {
									RoundedAvatarDrawable avtDrawable = new RoundedAvatarDrawable(
											bitmap);
									avt.setImageDrawable(avtDrawable);
								}
							}

						});

			}

			btnEdit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(),
							UpdateProfileActivity.class);
					intent.putExtra(constants.COMMON_KEY, userProfile);
					startActivity(intent);
				}
			});

			btnChangePass.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(getActivity(),
							ChangePassActivity.class);
					intent.putExtra(constants.COMMON_KEY, userProfile);
					startActivity(intent);
				}
			});

			btnLogout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Utils.logout(getActivity());
					listener.logout();
				}
			});

		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.profile_fragment_layout, null);
		btnLogout = (ButtonRectangle) view.findViewById(R.id.btn_logout);
		btnEdit = (ButtonRectangle) view.findViewById(R.id.btn_update);
		displayName = (EditText) view.findViewById(R.id.display_name);
		email = (EditText) view.findViewById(R.id.email);
		address = (EditText) view.findViewById(R.id.address);
		phone = (EditText) view.findViewById(R.id.phone);
		website = (EditText) view.findViewById(R.id.websites);
		avt = (ImageView) view.findViewById(R.id.avt);
		userName = (EditText) view.findViewById(R.id.user_name);
		btnChangePass = (ButtonRectangle) view.findViewById(R.id.btn_change_pass);
		return view;
	}

}
