package com.bk.lrandom.droidmarket.controller;

import android.content.Context;

import com.bk.lrandom.droidmarket.HomeActivity;
import com.bk.lrandom.droidmarket.R;
import com.bk.lrandom.droidmarket.business.UserSessionManager;
import com.bk.lrandom.droidmarket.business.Utils;
import com.bk.lrandom.droidmarket.models.User;

/**
 * Created by Andrea's on 7/7/2016.
 */
public class HomeController {
    private Context mContext;
    private HomeActivity mActivity;

    public HomeController(HomeActivity activity) {
        mActivity = activity;
        mContext = activity.getApplicationContext();
    }

}
