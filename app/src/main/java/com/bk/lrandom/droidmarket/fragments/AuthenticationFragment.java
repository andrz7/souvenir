package com.bk.lrandom.droidmarket.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bk.lrandom.droidmarket.CreateAccountActivity;
import com.bk.lrandom.droidmarket.LoginActivity;
import com.bk.lrandom.droidmarket.R;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.gc.materialdesign.views.ButtonRectangle;

@SuppressLint("NewApi")
public class AuthenticationFragment extends Fragment implements View.OnClickListener {
	private final String TAG = AuthenticationFragment.class.getSimpleName();

	private ButtonRectangle btnLogin, btnLoginAsGuest;
	private UiLifecycleHelper uiHelper;

	public static final AuthenticationFragment newInstance() {
		// TODO Auto-generated constructor stub
		AuthenticationFragment fragment = new AuthenticationFragment();
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.authentication_layout, container, false);
		btnLogin = (ButtonRectangle) view.findViewById(R.id.btn_login);
		btnLoginAsGuest = (ButtonRectangle) view.findViewById(R.id.btn_login_as_guest);

		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		btnLogin.setOnClickListener(this);
		btnLoginAsGuest.setOnClickListener(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.i("FB AUT FRAGMENT", "Logged in...");
		} else if (state.isClosed()) {
			Log.i("FB AUT FRAGMENT", "Logged out...");
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}

		uiHelper.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.btn_login:
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);
				break;
			case R.id.btn_login_as_guest:
				Intent i = new Intent(getActivity(), CreateAccountActivity.class);
				startActivity(i);
				break;
		}
	}
}
