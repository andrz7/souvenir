package com.bk.lrandom.droidmarket;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bk.lrandom.droidmarket.adapters.ShoppingCartAdapter;
import com.bk.lrandom.droidmarket.models.Cart;
import com.bk.lrandom.droidmarket.models.DummyData;

import java.util.ArrayList;

public class ShoppingCartActivity extends ActionBarParentActivity {
    private final String TAG = ShoppingCartActivity.class.getSimpleName();

    private TextView subItemtv, subTotaltv;
    private LinearLayout lastView;
    private RecyclerView cartrv;
    private Button submitBtn;
    private Toolbar toolbar;

    private LinearLayoutManager layoutManager;
    private ShoppingCartAdapter adapter;

    private ArrayList<Cart> carts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        cartrv = (RecyclerView) findViewById(R.id.cart_rv);
        lastView = (LinearLayout) findViewById(R.id.last_view);
        setToolbar();
        carts = new ArrayList<>();
        layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false
        );
        cartrv.setLayoutManager(layoutManager);
        carts = new DummyData("wa").dummyCart();
        Log.v(TAG, String.valueOf(carts.size()));
        adapter = new ShoppingCartAdapter(this, carts);
        cartrv.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Cart");
        toolbar.setElevation(20);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
